import argparse
import json


class Configuration:
    def __init__(self, json_obj):
        self.json_obj = json_obj

    def __getattr__(self, item):
        return self.__getitem__(item)

    def __getitem__(self, item):
        root = super().__getattribute__('json_obj')
        if '.' in item:
            keys = item.split('.')

            for key in keys:
                try:
                    root = root.get(key)
                except AttributeError:
                    exit("Non-existing argument!")

        else:
            root = root.get(item)

        if type(root) is dict:
            return Configuration(root)
        else:
            if root is None:
                exit("Wrong input!")
            return root

    def __str__(self):
        root = super().__getattribute__('json_obj')
        return str(root)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--format', type=str, help='You can run this program using json or jaml format.'
                                                 'Result is a correct value or a message if the argument is '
                                                 'non-existing')
    args = parser.parse_args()

    filepath = 'data.json'
    object = {}

    if args.format == "json":
        file = open('data.json')
        object = json.load(file)
    elif args.format == "yaml":
        with open(filepath, "r") as stream:
            try:
                object = filepath.safe_load(stream)
            except filepath.YAMLError as exc:
                print(exc)
    else:
        print("Wrong argument!")
        exit(0)

    config = Configuration(object)

    print(config.skills.cpp.tasks)
    # print(config.cpp.tasks)
    # print(config['skills.cpp.tasks'])
    # print(config.skills['cpp.tasks'])
    # print(config.skills.cpp)

    file.close()
