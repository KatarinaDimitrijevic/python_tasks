def input_operand(position):
    try:
        operand = float(input("Please enter the " + position + " operand: "))
        return operand
    except ValueError:
        print("Wrong input! The " + position + " operand must be a number!")


def input_operator():
    operator = input("Please enter the operator: +, -, * or /: ")
    if operator != '+' and operator != '-' and operator != '*' and operator != '/':
        return None
    return operator


if __name__ == '__main__':
    operand_1 = input_operand("first")
    while operand_1 is None:
        operand_1 = input_operand("first")

    operator = input_operator()
    while operator is None:
        print("Operator must be +, -, * or /, enter again!")
        operator = input_operator()

    operand_2 = input_operand("second")
    while operand_2 is None:
        operand_2 = input_operand("second")

    if operator == '+':
        result = operand_1 + operand_2
    if operator == '-':
        result = operand_1 - operand_2
    if operator == '*':
        result = operand_1 * operand_2
    if operator == '/':
        result = operand_1 / operand_2

    print(f"{operand_1} {operator} {operand_2} = {result}")
