import argparse
import contextlib
import json
import os
from _stat import S_ISREG
from stat import S_ISDIR

from dict2xml import dict2xml


def order_by(current_dir_content, options):
    if options.order == "modified_time":
        current_dir_content.sort(key=lambda x: os.stat(x[1]).st_mtime, reverse=True)
    if options.order == "size":
        current_dir_content.sort(key=lambda x: int(os.stat(x[1]).st_size))
    if options.order == "access_time":
        current_dir_content.sort(key=lambda x: os.stat(x[1]).st_atime, reverse=True)

    return current_dir_content


def list_files_and_dirs(options):
    current_dir_content = []

    level = options.level

    if level is None:
        level = 0

    walk_tree('.', current_dir_content, "", level)
    current_dir_content = filter_dir_or_file(current_dir_content, options)
    current_dir_content = order_by(current_dir_content, options)

    return current_dir_content


def filter_dir_or_file(curr_dir_content, options):
    list_of_files = []

    for f in curr_dir_content:
        mode = os.stat(f[1]).st_mode

        if options.filter == "directory":
            if S_ISDIR(mode):
                list_of_files.append(f)
        elif options.filter == "file":
            if S_ISREG(mode):
                list_of_files.append(f)

        elif options.filter is None:
            list_of_files = curr_dir_content

    return list_of_files


def walk_tree(root, current_dir_content, option="", level=0):
    if level < 0:
        return

    for current_path in os.listdir(root):
        pathname = os.path.join(root, current_path)
        mode = os.lstat(pathname).st_mode

        current_dir_content.append((current_path, pathname, level))

        if S_ISDIR(mode):
            walk_tree(pathname, current_dir_content, "", level - 1)


def extract_details(file_stats, options):
    stats = []

    levels = [i[2] for i in file_stats]

    for entry in file_stats:
        stat_object = os.stat(entry[1])
        stats.append(
            {entry[0]: {attr: getattr(stat_object, attr) for attr in dir(stat_object) if attr.startswith('st_')}}
        )

    if options.save_output is not None:
        file_name = options.save_output.split(" ")[0]
        option = options.save_output.split(" ")[1]
        with open(file_name, option) as o:
            with contextlib.redirect_stdout(o):
                display_info(stats, options, levels)

    else:
        display_info(stats, options, levels)


def display_info(stats, options, levels):
    if options.format == "xml":
        print("Info in xml format: ")
        for x in stats:
            display_using_xml(stats)
        print()
    elif options.format == "json":
        print("Info in json format: ")
        display_using_json(stats)
        print()
    elif options.format is None:
        for i, element in enumerate(stats):
            for key_outer in element:
                print('\t' * (options.level - levels[i]) + key_outer)
                print('\t' * (options.level - levels[i]) + '_' * len(key_outer))
                for key_inner in element[key_outer]:
                    print('\t' * (options.level - levels[i]) + key_inner + ": " + str(element[key_outer][key_inner]))
                print()


def display_using_json(stats):
    json_object = json.dumps(stats, indent=len(stats) + 1)
    print(json_object)


def display_using_xml(stats):
    xml_object = dict2xml(stats)
    print(xml_object)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--details', type=str)
    parser.add_argument('--order', type=str)
    parser.add_argument('--filter', type=str)
    parser.add_argument('--format', type=str)
    parser.add_argument('--level', type=int)
    parser.add_argument('--save_output', type=str)

    args = parser.parse_args()
    print(args)
    listed_files = list_files_and_dirs(args)

    print()

    if args.details == 'y':
        extract_details(listed_files, args)
    else:
        for f in listed_files:
            print("\t" * (args.level - f[2]) + f[0])
